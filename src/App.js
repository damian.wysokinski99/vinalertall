import { BrowserRouter as Router, Switch, Route, HashRouter  } from 'react-router-dom';
import './App.css';
import Footer from './components/Footer';
import Navbar from './components/Navbar';
import Home from './components/pages/HomePage/Home';
import Report from './components/pages/ReportPage/Report';
import Terms from './components/pages/TermsPages/Terms';
import Privacy from './components/pages/TermsPages/Privacy';
import Affiliate from './components/pages/TermsPages/Affiliate';

function App() {
  return (
    <HashRouter>
      <Navbar />
      <Switch>
        <Route path='/' exact component={Home} />
        <Route path='/page/:title' component={Report} />
        <Route path='/terms' component={Terms} />
        <Route path='/privacy' component={Privacy} />
        <Route path='/affiliate' component={Affiliate} />
      </Switch>
      <Footer />
    </HashRouter>
  );
}

export default App;
