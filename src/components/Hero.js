import React, { useState } from 'react';
import './Hero.css';
import car from '../assets/car.svg';
import { FaSearch, FaStar } from "react-icons/fa";
import { Link } from 'react-router-dom'

function Hero() {
    const [title, setTitle] = useState('');

    function decodeVin(){
        alert("VIN is too short, minimum 17 characters.");
    }
    
    return (
        <>
            <div className="hero">
                <div className="row container">
                    <div className="col">
                        <h1>
                            Check Your Next <span className="primary-text">Car</span>
                            <br /> Before Buying.
                        </h1>
                        <p className="sub-title">
                            Decode Your <span className="primary-text"> Vehicle Identification</span> Number for <span className="primary-text">Free!</span>
                        </p>

                        <div className="hero-box">
                            <label>Search for Car Report:</label>

                            <form>
                                <input 
                                    onChange={event => setTitle(event.target.value)} 
                                    onKeyPress={(e) => { e.key === 'Enter' && e.preventDefault(); }}
                                    type="text" 
                                    placeholder="Enter VIN Number" 
                                    maxLength="17" 
                                    name="vin" 
                                />

                                {title.length < 12 ?
                                <Link 
                                onClick={decodeVin}
                                className="decode-button"
                                >
                                <button 
                                    className="btn primary"
                                    type="button"
                                >
                                <FaSearch className="search-icon" />CHECK NOW</button>
                                </Link>
                                :
                                <Link 
                                to={`/page/`+title}
                                className="decode-button"
                                >
                                <button 
                                    className="btn primary"
                                    type="button"
                                >
                                <FaSearch className="search-icon" />CHECK NOW</button>
                                </Link>
                                }
                            </form>
                        </div>

                        <div className="stars">
                            <p>4/5</p>
                            <FaStar className="star primary-text"/>
                            <FaStar className="star primary-text" />
                            <FaStar className="star primary-text" />
                            <FaStar className="star primary-text" />
                            <FaStar className="star star-black" /> 
                            <p>by 24613 clients.</p>
                        </div>
                    </div>
                    <div className="col hero-img">
                        <img src={car} alt=""/>
                    </div>
                </div>
            </div>
        </>
    )
}

export default Hero
