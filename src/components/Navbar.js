import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { FaBars, FaTimes } from 'react-icons/fa'
import { Button } from './Button'
import './Navbar.css';

function Navbar() {
    const [click, setClick] = useState(false);
    const [button, setButton] = useState(true);

    const handleClick = () => setClick(!click);
    const closeMobileMenu = () => setClick(false);

    const showButton = () => {
        if(window.innerWidth <= 960) {
            setButton(false)
        } else {
            setButton(true)
        }
    }

    window.addEventListener('resize', showButton);

    return (
        <>
            <div className="navbar">
                <div className="navbar-container container">

                    <Link to='/' className="navbar-logo">
                        vinalertall
                    </Link>

                    <div className="nav-btn nav-menu">
                        <a href='https://47a147qlp3mq609gw8p4vzfs7c.hop.clickbank.net/' className="btn-link">
                            <Button buttonStyle='btn--50' buttonColor='primary'>Get Report</Button>
                        </a>
                    </div>

                </div>
            </div>
        </>
    )
}

export default Navbar
