import React from 'react';
import './Pricing.css';
import { Button } from './Button';
import Businessman from '../assets/Businessman.svg'
import Manager from '../assets/Manager.svg'

function Pricing({ pricingTitle }) {
    return (
        <div className="pricing">
            <div className="container">
            <h2>{pricingTitle}</h2>
                <div className="row">
                    <div className="col">
                        <div className="pricing-card">

                            <div className="card-img">
                                <img src={Businessman} alt=""/>
                            </div>

                            <div className="price">
                                <h3><span>$</span>9.95</h3>
                                <p>Report</p>
                            </div>

                            <a href='https://47a147qlp3mq609gw8p4vzfs7c.hop.clickbank.net/'>
                                <Button>GET REPORT</Button>
                            </a>
                        </div>
                    </div>
                    <div className="col">
                        <div className="pricing-card">

                            <div className="card-img">
                                <img src={Manager} alt=""/>
                            </div>

                            <div className="price">
                                <h3><span>$</span>15.95</h3>
                                <p>5 Reports</p>
                            </div>

                            <a href='https://47a147qlp3mq609gw8p4vzfs7c.hop.clickbank.net/'>
                                <Button>GET REPORTS</Button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Pricing
