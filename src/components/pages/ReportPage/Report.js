import React, { useState, useEffect } from 'react';
import Pricing from '../../Pricing';
import './Report.css';
import Loader from '../../Loader';

function Report() {
    const [people, setPeople] = useState([]);
    const [loading, setLoading] = useState(true);
    const data = people.Results;

    useEffect(() => {
        setLoading(true);
        fetch(`https://vpic.nhtsa.dot.gov/api/vehicles/decodevinvalues/`+vin+`?format=json`)
          .then((res) => res.json())
          .then((res) => {
            setPeople(res);
            setLoading(false);
          });
    }, []);

    let urlElements = window.location.href.split('/');
    let vin = (urlElements[5]);
    document.title = vin+" | Free Car History Report by VIN Number";

    if(loading) return <Loader />

    return (
        <>
          <div className="hin-check__title" id="test">
              <div className="container">
                {data[0].Make === "" ? 
                  <h2><span>Error!</span> We did not found any records for this vehicle:</h2>
                :
                  <h2><span>Success!</span> We found records for this vehicle:</h2>
                }
              </div>     
          </div>
          

            <div className="container">
              {data[0].Make === "" ?
              <>
              <div className="report-header">
                <h2><span>VIN: </span>{vin}</h2>
                <h2><span>MODEL: </span>-</h2>
              </div>
              <table className="report-table">
                <tr><td><b>Used VIN</b></td><td>{vin}</td></tr>
                <tr><td><b>Make</b></td><td>-</td></tr>
                <tr><td><b>Model Year</b></td><td>-</td></tr>
              </table>
              </>
              :
              <>
              <div className="report-header">
                <h2><span>VIN: </span>{vin}</h2>
                <h2><span>MODEL: </span>{data[0].Model}</h2>
              </div>
              <table className="report-table">
                <tbody>
                  <tr><td><b>Used VIN</b></td><td>{vin}</td></tr>
                  <tr><td><b>Make</b></td><td>{data[0].Make}</td></tr>
                  <tr><td><b>Model Year</b></td><td>{data[0].ModelYear}</td></tr>
                  <tr><td><b>ABS</b></td><td>{data[0].ABS}</td></tr>
                  <tr><td><b>AEB</b></td><td>{data[0].AEB}</td></tr>
                  <tr><td><b>BedType</b></td><td>{data[0].BedType}</td></tr>
                  <tr><td><b>BusFloorConfigType</b></td><td>{data[0].BusFloorConfigType}</td></tr>
                  <tr><td><b>BusLength</b></td><td>{data[0].BusLength}</td></tr>
                  <tr><td><b>BusType</b></td><td>{data[0].BusType}</td></tr>
                  <tr><td><b>Country</b></td><td>{data[0].Country}</td></tr>
                  <tr><td><b>CurbWeightLB</b></td><td>{data[0].CurbWeightLB}</td></tr>
                  <tr><td><b>CustomMotorcycleType</b></td><td>{data[0].CustomMotorcycleType}</td></tr>
                  <tr><td><b>DestinationMarket</b></td><td>{data[0].DestinationMarket}</td></tr>
                  <tr><td><b>DisplacementCC</b></td><td>{data[0].DisplacementCC}</td></tr>
                  <tr><td><b>DisplacementCI</b></td><td>{data[0].DisplacementCI}</td></tr>
                  <tr><td><b>DisplacementL</b></td><td>{data[0].DisplacementL}</td></tr>
                  <tr><td><b>Doors</b></td><td>{data[0].Doors}</td></tr>
                  <tr><td><b>DriveType</b></td><td>{data[0].DriveType}</td></tr>
                  <tr><td><b>DriverAssist</b></td><td>{data[0].DriverAssist}</td></tr>
                  <tr><td><b>EngineConfiguration</b></td><td>{data[0].EngineConfiguration}</td></tr>
                  <tr><td><b>EngineCycles</b></td><td>{data[0].EngineCycles}</td></tr>
                  <tr><td><b>EngineCylinders</b></td><td>{data[0].EngineCylinders}</td></tr>
                  <tr><td><b>EngineHP</b></td><td>{data[0].EngineHP}</td></tr>
                  <tr><td><b>EngineHP_to</b></td><td>{data[0].EngineHP_to}</td></tr>
                  <tr><td><b>EngineKW</b></td><td>{data[0].EngineKW}</td></tr>
                  <tr><td><b>EngineManufacturer</b></td><td>{data[0].EngineManufacturer}</td></tr>
                  <tr><td><b>EngineModel</b></td><td>{data[0].EngineModel}</td></tr>
                  <tr><td><b>ManufacturerId</b></td><td>{data[0].ManufacturerId}</td></tr>
                  <tr><td><b>ManufacturerType</b></td><td>{data[0].ManufacturerType}</td></tr>
                  <tr><td><b>Model</b></td><td>{data[0].Model}</td></tr>
                  <tr><td><b>MotorcycleChassisType</b></td><td>{data[0].MotorcycleChassisType}</td></tr>
                  <tr><td><b>MotorcycleSuspensionType</b></td><td>{data[0].MotorcycleSuspensionType}</td></tr>
                  <tr><td><b>OtherBusInfo</b></td><td>{data[0].OtherBusInfo}</td></tr>
                  <tr><td><b>OtherEngineInfo</b></td><td>{data[0].OtherEngineInfo}</td></tr>
                  <tr><td><b>OtherMotorcycleInfo</b></td><td>{data[0].OtherMotorcycleInfo}</td></tr>
                  <tr><td><b>OtherRestraintSystemInfo</b></td><td>{data[0].OtherRestraintSystemInfo}</td></tr>
                  <tr><td><b>OtherTrailerInfo</b></td><td>{data[0].OtherTrailerInfo}</td></tr>
                  <tr><td><b>PlantCity</b></td><td>{data[0].PlantCity}</td></tr>
                  <tr><td><b>PlantCompanyName</b></td><td>{data[0].PlantCompanyName}</td></tr>
                  <tr><td><b>PlantCountry</b></td><td>{data[0].PlantCountry}</td></tr>
                  <tr><td><b>PlantState</b></td><td>{data[0].PlantState}</td></tr>
                  <tr><td><b>SuggestedVIN</b></td><td>{data[0].SuggestedVIN}</td></tr>
                  <tr><td><b>TopSpeedMPH</b></td><td>{data[0].TopSpeedMPH}</td></tr>
                  <tr><td><b>TractionControl</b></td><td>{data[0].TractionControl}</td></tr>
                  <tr><td><b>TrailerBodyType</b></td><td>{data[0].TrailerBodyType}</td></tr>
                  <tr><td><b>TrailerLength</b></td><td>{data[0].TrailerLength}</td></tr>
                  <tr><td><b>TrailerType</b></td><td>{data[0].TrailerType}</td></tr>
                  <tr><td><b>TransmissionSpeeds</b></td><td>{data[0].TransmissionSpeeds}</td></tr>
                  <tr><td><b>TransmissionStyle</b></td><td>{data[0].TransmissionStyle}</td></tr>
                  <tr><td><b>Turbo</b></td><td>{data[0].Turbo}</td></tr>
                  <tr><td><b>ValveTrainDesign</b></td><td>{data[0].ValveTrainDesign}</td></tr>
                  <tr><td><b>VehicleType</b></td><td>{data[0].VehicleType}</td></tr>
                </tbody>
              </table>
              </>
              }
            </div>
        </>
    )
}

export default Report
