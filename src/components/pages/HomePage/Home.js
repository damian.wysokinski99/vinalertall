import React from 'react'
import { Link } from 'react-router-dom';

import Hero from '../../Hero'
import Pricing from '../../Pricing'

import '../../MayShow.css';
import '../../BetterWay.css';

import report from '../../../assets/report.svg';

import step1 from '../../../assets/step-1.svg';
import step2 from '../../../assets/step-2.svg';
import step3 from '../../../assets/step-3.svg';

import { FaCheckCircle } from "react-icons/fa";

import Card from '../../Card';
import { Button } from '../../Button';

function Home() {

    document.title = "Home - Vinalertall"

    const Steps = [
        { id: 1, title: "Enter VIN", body: "The only thing you need to know. Easily found in vehicle documents.", imageStep: step1},
        { id: 2, title: "Data Search", body: "We search information from official sources.", imageStep: step2},
        { id: 3, title: "Get Report", body: "Get full car history and reduce anxiety about the deal.", imageStep: step3}
    ];

    return (
        <>
            <Hero />
            <div className="may-show">
                <div className="row container">
                    <div className="col">
                        <img className="report-img" src={report} alt=""/>
                    </div>
                    <div className="col">
                        <div className="may-show-wrapper">
                        <h2>What Car <span className="primary-text">History Report</span><br /> May Show?</h2>

                        <ul>
                            <li><FaCheckCircle className="primary-text"/> Vehicle service history</li>
                            <li><FaCheckCircle className="primary-text"/> Mileage rollback</li>
                            <li><FaCheckCircle className="primary-text"/> Multiple owners</li>
                            <li><FaCheckCircle className="primary-text"/> Common faults of each model</li>
                            <li><FaCheckCircle className="primary-text"/> Hidden damage report</li>
                            <li><FaCheckCircle className="primary-text"/> Other useful information</li>
                        </ul>
                        </div>
                    </div>
                </div>
            </div>

            <div className="better-way">
            <div className="container">
            <h2>Better Way to Find Your Next Car</h2>
                <div className="row">
                    {Steps.map(item => (
                    <Card 
                        key={item.id}
                        title={item.title}
                        imageUrl={item.imageStep}
                        body={item.body}
                    />
                    ))}
                </div>

                <div className="better-way-btn">
                    <a href='https://47a147qlp3mq609gw8p4vzfs7c.hop.clickbank.net/'>
                        <Button buttonSize="btn--large">Check VIN Number</Button>
                    </a>
                </div>
            </div>
            </div>
            <Pricing pricingTitle="Pricing" />
        </>
    )
}

export default Home
