import React from 'react'

function Terms() {
    document.title = "Terms & Conditions - Vinalertall"

    return (
        <div className="terms-body">
            <div className="container">
                <h2>Terms & Conditions</h2>
                <p>These terms of use govern your use of our site. These website terms of use have been provided and, approved by the solicitors at legal documents resource DIY Legals. Please read these terms in full before you use this Website. If you do not accept these terms of use, please do not use this Website. Your continued use of this site confirms your acceptance of these terms.
            <br /><br />
            WEB SITE ACCESS
            <br /><br />
            1.1 It is not necessary to register with us in order to use most parts of this Website. However, particular areas of this Website will only be accessible only if you have registered.
            <br /><br />
            USE OF WEBSITE
            <br /><br />
            1.2 This Website may be used for your own private purposes and in accordance with these terms of use.
            <br /><br />
            1.3 You may print and download material from this Website provided that you do not modify or reproduce any content without our prior written consent.
            <br /><br />
            SITE UPTIME
            <br /><br />
            1.4 All reasonable measures are taken by us to ensure that this Website is operational all day, every day. However, occasionally technical issues may result in some downtime, and accordingly, we will not be liable if this website is unavailable at any time.
            <br /><br />
            1.5 Where possible we always try to give advance warning of maintenance issues that may result in Website down time but we shall not be obliged to provide such notice.
            <br /><br />
            VISITOR PROVIDED MATERIAL
            <br /><br />
            1.6 Any material that a visitor to this Website sends or posts to this Website shall be considered non-proprietary and non-confidential. We shall be entitled to copy, disclose, distribute or use for such other purposes as we deem appropriate all material provided to us, with the exception of personal information, the use of which is covered under our Privacy Policy.
            <br /><br />
            1.7 When using this website you shall not post or send to or from this Website any material:
            <br /><br />
            (a) for which you have not obtained all necessary consents;
            <br /><br />
            (b) that is discriminatory, obscene, pornographic, defamatory, liable to incite racial hatred, in breach of confidentiality or privacy, which may cause annoyance or inconvenience to others, which encourages or constitutes conduct that would be deemed a criminal offence, give rise to a civil liability, or otherwise is contrary to the law in the United Kingdom;
            <br /><br />
            (c) which is harmful in nature including, and without limitation, computer viruses, Trojan horses, corrupted data, or other potentially harmful software or data.
            <br /><br />
            1.8 We will fully co-operate with any law enforcement authorities or court order requiring us to disclose the identity or other details of any person posting material to this website in breach of Paragraph1.7.
            <br /><br />
            LINKS TO AND FROM OTHER WEBSITES
            <br /><br />
            1.9 Throughout this Website you may find links to third party websites. The provision of a link to such a website does not mean that we endorse that website. If you visit any website via a link on this Website you do so at your own risk.
            <br /><br />
            1.10 Any party wishing to link to this website is entitled to do so provided that the conditions below are observed:
            <br /><br />
            (a) you do not seek to imply that we are endorsing the services or products of another party unless this has been agreed with us in writing;
            <br /><br />
            (b) you do not misrepresent your relationship with this website; and
            <br /><br />
            (c) the website from which you link to this Website does not contain offensive or otherwise controversial content or, content that infringes any intellectual property rights or other rights of a third party.
            <br /><br />
            1.11 By linking to this Website in breach of clause 5.2 you shall indemnify us for any loss or damage suffered to this Website as a result of such linking.
            <br /><br />
            DISCLAIMER
            <br /><br />
            1.12 Whilst we do take all reasonable steps to make sure that the information on this website is up to date and accurate at all times we do not guarantee that all material is accurate and, or up to date.
            <br /><br />
            1.13 All material contained on this Website is provided without any or warranty of any kind. You use the material on this Website at your own discretion.
            <br /><br />
            EXCLUSION OF LIABILITY
            <br /><br />
            1.14 We do not accept liability for any loss or damage that you suffer as a result of using this Website.
            <br /><br />
            1.15 Nothing in these Terms of Use shall exclude or limit liability for death or personal injury caused by negligence which cannot be excluded or under the law of the United Kingdom.
            <br /><br />
            1.16 The information on this website, unless specifically stated, has not been written by a professional, nor is it provided with the intention of providing professional advice on any topic.
            <br /><br />
            1.17&nbsp;THE SITE DOES NOT PROVIDE PROFESSIONAL MEDICAL, FITNESS, OR NUTRITION ADVICE. This website is based on personal experience and is designed to provide information about the subject matter covered. Every effort has been made to make it as complete and accurate as possible. All information provided through this website, its products, and associated video program and websites are for informational purposes only and are not intended to replace the care, advice, or instruction of a medical professional.
            <br /><br />
            Its author, or company, will not be held liable in any way for the information contained through the website or this product. Users should consult a physician before making any lifestyle, dietary, or other health or psychologically related changes. This website and associated products solely represent their author’s opinion. You are responsible for your own behavior, and none of the content is to be considered personal, financial, psychological, or medical advice. Results will vary for individual users.
            <br /><br />
            LAW and JURISDICTION
            <br /><br />
            These terms of use are governed by English law. Any dispute arising in connection with these terms of use shall be subject to the exclusive jurisdiction of the Courts of England and Wales.</p>
            </div>
        </div>
    )
}

export default Terms
