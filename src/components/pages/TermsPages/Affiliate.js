import React from 'react'

function Affiliate() {
    document.title = "Affilaite Disclaimer - Vinalertall"

    return (
        <div className="terms-body">
            <div className="container">
            <h2>Affiliate Disclaimer</h2>
            <p>I think transparency is important. For that reason, I’m disclosing that if you buy some of the products or services on this site the site owners may receive affiliate compensation. This could be in the form of free products, monetary compensation, or something else. This won’t affect the price you pay.
            <br /><br />
            For this reason, you should assume all&nbsp;links on this site are&nbsp;an affiliate link unless otherwise stated.
            <br /><br />
            This compensation disclosure is designed to provide you with protection and disclose any relationship between the writers or owners of vinalertall.com and products and services on our site.
            <br /><br />
            Clickbank Disclaimer
            <br /><br />
            vinalertall.com is a participant in the Clickbank Program, an affiliate advertising program designed to provide a means for sites to earn advertising fees by advertising and linking to Clickbank.com.
            <br /><br />
            Additional Information
            <br /><br />
            If you visit one of our affiliate partners, a cookie will be set within your web browser.&nbsp;IF&nbsp;you purchase a product, we receive a commission. This is a common way for websites to monetize content and is entirely legitimate.
            We may sometimes shorten affiliate links so that we can track how many of our visitors are clicking on them. This is not to hide our relationships with our affiliate partners. Again, link shortening is a common and legitimate practice.
            We do NOT receive payment to WRITE the reviews or information on this site. However, we must disclose that our affiliate relationships may create a conflict of interest and could influence the reviews and posts. We do not believe our reviews are influenced, but it’s up to you – the visitor – to decide whether to buy a product.
            We only review products that we believe we can trust to provide an excellent service. We strive to write honest beliefs, experiences, and opinions on the products we review.</p>
            </div>
        </div>
    )
}

export default Affiliate
