import React from 'react'
import { Link } from 'react-router-dom'
import { FaFacebook, FaInstagram } from 'react-icons/fa'
import './Footer.css';

function Footer() {
    return (
        <div className="footer">
            <div className="container">
                <div className="row">

                    <div className="col col-center">
                        <Link to='/' className="footer-logo">
                            vinalertall
                        </Link>
                    </div>

                    <div className="col col-center">
                        <ul>
                            <a href='https://hinchecker.com'><li>HIN History Report</li></a>
                            <Link to='/terms'><li>Terms & Conditions</li></Link>
                            <Link to='/privacy'><li>Privacy Policy</li></Link>
                            <Link to='/affiliate'><li>Affiliate Disclaimer</li></Link>
                        </ul>
                    </div>
                    <div className="col col-center">
                        <div className="social">
                            <p>Find us on:</p>
                            <FaFacebook className="footer-icon" />
                            <FaInstagram className="footer-icon" />
                        </div>
                    </div>
                </div>

                <div className="copyright">
                    <p>© COPYRIGHT {(new Date().getFullYear())} - All rights reserved</p>
                </div>
            </div>
        </div>
    )
}

export default Footer
