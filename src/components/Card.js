import React from 'react';
import './Card.css';

function Card({ title, imageUrl, body }) {
    return (
        <div className="card">
            <img src={imageUrl} alt=""/>
            
            <div className="card-title">
                {title}
            </div>

            <div className="card-body">
                {body}
            </div>
        </div>
    )
}

export default Card
